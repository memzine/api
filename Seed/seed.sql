DROP TABLE IF EXISTS entries;
DROP TABLE IF EXISTS users;
CREATE TABLE users (
	id SERIAL PRIMARY KEY,
	username TEXT NOT NULL UNIQUE
);

INSERT INTO users (username)
	VALUES 
	('theomjones'),
	('sallyjo')
;

DROP TABLE IF EXISTS entries;
CREATE TABLE entries (
	id SERIAL PRIMARY KEY,
	authorid integer NOT NULL REFERENCES users(id),
	content text NOT NULL,
	created timestamp NOT NULL DEFAULT now(),
	updated timestamp DEFAULT null
);

INSERT INTO entries (authorid, content)
	VALUES 
		(1, 'I am some random content to start'),
		(1, 'I am some more random content'),
		(2, 'I am content from sally. :)'),
		(2, 'I am content from sally. :)')
;