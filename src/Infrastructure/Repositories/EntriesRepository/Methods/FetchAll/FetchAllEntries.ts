import Entry from '../../../../../Models/Entry/EntryModel'
import { Models } from '../../../../../Typings/Types'
import NotFoundError from '../../../../../Errors/NotFound'

/**
 * Fetch entries where user id = author id 
 */

const fetchAllEntries = async (user: Models.IUser): Promise<Models.IEntry[]> => {
    try {
        const docs = await Entry.find({ authorId: user._id })
        return docs
    } catch (error) {
        throw new NotFoundError()
    }
}

export default fetchAllEntries