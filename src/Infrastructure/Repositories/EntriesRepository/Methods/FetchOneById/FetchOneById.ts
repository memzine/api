import Entry from '../../../../../Models/Entry/EntryModel'
import { Models } from '../../../../../Typings/Types'
import NotFoundError from '../../../../../Errors/NotFound'
import NotAuthorizedError from '../../../../../Errors/Authentication/NotAuthorized'

const fetchOneById = async (id: number, user: Models.IUser): Promise<Models.IEntry> => {
    try {
        const doc = await Entry.findById(id)
        // Validate User then return
        if (doc.authorId.equals(user._id)) {
            return doc
        } else {
            throw new NotAuthorizedError(new Error('You are not authorized to access that entry.'))
        }
    } catch (error) {
        if (error.code === 401) {
            throw error
        } else {
            throw new NotFoundError()
        }
    }
}

export default fetchOneById