import Entry from '../../../../../Models/Entry/EntryModel'
import { Models } from '../../../../../Typings/Types'
import WriteError from '../../../../../Errors/ReadWrite/write'

const saveEntry = async (entry: Models.IEntry) => {
    // if ((entry instanceof Entry) === false) { throw new BadRequestError('Bad data')}
    try {
        const newEntry = new Entry(entry)
        const doc = await newEntry.save()
        return doc
    } catch (error) {
        throw error
    }
}

export default saveEntry