import { Models, EntryModels } from '../../../../../Typings/Types'
import Entry from '../../../../../Models/Entry/EntryModel'
import NotAuthorizedError from '../../../../../Errors/Authentication/NotAuthorized'

/**
 * Patch Entry
 */

const updateOne = async (id: string, data: any, user: Models.IUser): Promise<Models.IEntry> => {
  try {
    // Currently this authorizes by matching authorId to user.id from token.
    // Probably should be a bit more robust?
    const updated = await Entry.findOneAndUpdate({
      _id: id,
      authorId: user.id
    }, data, { new: true })
    if (updated) {
      return updated
    } else {
      throw new NotAuthorizedError(new Error('You cannot update this entry because you do not own it.'))
    }
  } catch (error) {
    throw error
  }
}

export default updateOne
