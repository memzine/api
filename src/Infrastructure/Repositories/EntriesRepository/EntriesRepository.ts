import fetchOneById from './Methods/FetchOneById/FetchOneById'
import saveEntry from './Methods/SaveEntry/SaveEntry'
import fetchAllEntries from './Methods/FetchAll/FetchAllEntries'
import updateOne from './Methods/UpdateOne/UpdateOne'

const entriesRepository = {
    findById: fetchOneById,
    saveOne: saveEntry,
    fetchAll: fetchAllEntries,
    updateOne
}

export default entriesRepository