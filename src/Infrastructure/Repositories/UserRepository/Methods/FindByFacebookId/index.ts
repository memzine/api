import User from '../../../../../Models/User/UserModel'
import { Models } from '../../../../../Typings/Types'
import NotFoundError from '../../../../../Errors/NotFound'

const findOneByFacebookId =  async (fbId: string): Promise<Models.IUser> => {
  try {
    const user = await User.findOne({ 'facebookProvider.id': fbId }).exec()
    if (!user) { throw new NotFoundError() }
    return user
  } catch (error) {
    throw new NotFoundError('Could not find a user with that Facebook ID.')
  }
}

export default findOneByFacebookId