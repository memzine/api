import User from '../../../../../Models/User/UserModel'
import { Models, UserModels } from '../../../../../Typings/Types'
import NotFoundError from '../../../../../Errors/NotFound'

// TODO: IFacebookProfileResponse

const fetchOneByFacebookIdOrCreate =  async (profile: any): Promise<Models.IUser> => {
  try {
    const user = await User.findOne({ 'facebookProvider.id': profile.id })
    // if user does not exist create one
    if (!user) {
      const newUser: Models.IUser = new User({
        username: profile.emails[0].value.substr(0, profile.emails[0].value.indexOf('@')),
        email: profile.emails[0].value,
        role: 'user',
        facebookProvider: profile
      })
      const savedUser = await newUser.save()
      return savedUser
    } else {
      return user
    }
  } catch (error) {
    throw error
  }
}

export default fetchOneByFacebookIdOrCreate