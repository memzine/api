import { UserModels } from '../../../../../Typings/Types'
import User from '../../../../../Models/User/UserModel'
import BadRequestError from '../../../../../Errors/BadRequest'

const saveOne = async (user: UserModels.IUserObject) => {
  try {
    const userDoc = new User(user)
    const newDoc = await userDoc.save()
    return newDoc
  } catch (error) {
    // specific to mongodb
    if (error.code === 11000) {
      throw new BadRequestError('A user with that email/username already exists.')
    }
    throw error
  }
}

export default saveOne