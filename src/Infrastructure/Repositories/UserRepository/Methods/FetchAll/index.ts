import User, { hiddenFields } from '../../../../../Models/User/UserModel'
import { Models } from '../../../../../Typings/Types'
import NotAuthorizedError from '../../../../../Errors/Authentication/NotAuthorized'

export default async (privateData?: boolean): Promise<Models.IUser[]> => {
  let pick: {}
  if (privateData) {
    pick = hiddenFields
  }
  try {
    const user = await User.find({}, pick).exec()
    if (!user) {
      throw new NotAuthorizedError(new Error('You are not authorized.'))
    } else {
      return user
    }
  } catch (error) {
    throw new NotAuthorizedError(new Error('You are not authorized.'))
  }
}