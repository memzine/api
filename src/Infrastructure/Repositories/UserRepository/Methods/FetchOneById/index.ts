import User from '../../../../../Models/User/UserModel'
import { Models } from '../../../../../Typings/Types'
import NotAuthorizedError from '../../../../../Errors/Authentication/NotAuthorized'

export default async (id: string): Promise<Models.IUser> => {
  try {
    const user = await User.findById(id).exec()
    if (!user) {
      throw new NotAuthorizedError(new Error('You are not authorized.'))
    } else {
      return user
    }
  } catch (error) {
    throw new NotAuthorizedError(new Error('You are not authorized.'))
  }
}