import User from '../../../../../Models/User/UserModel'
import { Document } from 'mongoose'
import { Models } from '../../../../../Typings/Types'
import NotFoundError from '../../../../../Errors/NotFound'

const fetchOneByEmail = async (email: string): Promise<Models.IUser> => {
  try {
    return await User.findOne({ email }) 
  } catch (error) {
    throw new NotFoundError('Could not find user.')
  }
}

export default fetchOneByEmail