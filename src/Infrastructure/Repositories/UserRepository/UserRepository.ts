import fetchOneByEmail from './Methods/FetchOneByEmail/FetchOneByEmail'
import fetchOneById from './Methods/FetchOneById'
import addOne from './Methods/SaveOne/SaveOne'
import findOneByFacebookId from './Methods/FindByFacebookId/index'
import fetchOneByFacebookIdOrCreate from './Methods/FetchOneByFacebookIdOrCreate/index'
import fetchAll from './Methods/FetchAll'

export default {
  fetchOneByEmail,
  addOne,
  fetchOneById,
  fetchOneByFacebookId: findOneByFacebookId,
  fetchOneByFacebookIdOrCreate,
  fetchAll
}