import * as cors from 'cors'
import * as express from 'express'
import { DB_URL, PORT, EMAIL_URL } from '../config'

// Remove for prod
import * as morgan from 'morgan'
import * as ip from 'ip'
import * as path from 'path'
import * as qrcode from 'qrcode-terminal'

// ROUTERS
import entriesRouter from '../Router/Entries/EntriesRouter'
import tokenRouter from '../Router/Token/TokenRouter'
import userRouter from '../Router/User/UserRoutes'
import authRouter from '../Router/Auth/index'

// PASSPORT
import passportConfig from '../Authentication/Passport/index'

// ERROR MIDDLEWARE
import errorHandler from '../Middleware/Error'

export const app = express()

// Passport
passportConfig(app)
// Config
app.use(cors({
    origin: 'http://localhost:8080',
    credentials: true,
    methods: ['GET', 'POST', 'UPDATE', 'DELETE', 'PATCH']
}))
app.use(morgan('dev'))

// Add Routes
app.use('/entry', entriesRouter)
app.use('/token', tokenRouter)
app.use('/user', userRouter)
app.use('/auth', authRouter)

// ERRORS
app.use(errorHandler)

// SINGLE ROUTE
app.get('/', (req: express.Request, res: express.Response) => {
    res.sendFile(path.join(__dirname + '/../../readme.html'))
})

// Listen
app.listen(PORT, (req: express.Request, res: express.Response) => {
    if (process.env.NODE_ENV === 'dev' || process.env.NODE_ENV === 'test') {
        dev.printServer()
    } else {
        console.log('Listening on: ', PORT)
    }
})

// DELETE @ PRODUCTION
const dev = {
    printServer(): void {
        console.log('-'.repeat(70))
        console.log(`ENVIRONMENT: ${process.env.NODE_ENV}`)
        console.log('---')
        console.log(`Address: http://${ip.address()}:${PORT}`)
        console.log('---')
        console.log(`PORT: ${PORT}`)
        console.log('---')
        console.log(`DATABASE URL: ${DB_URL}`)
        console.log('---')
        console.log(`EMAIL URL: ${EMAIL_URL}`)
        console.log('-'.repeat(70))
        // qrcode.generate(`http://${ip.address()}:${PORT}`)
    }
}
