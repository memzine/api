import { Document } from 'mongoose'
import { Request } from 'express'


export namespace UserModels {
    export interface IUserObject {
        email: string
        username: string
    }
}

export namespace EntryModels {
    export interface IEntryObject {
        content: string
        tags: string[]
        emojis: string[]
        authorId: string
    }
}

export namespace Models {
    interface IAuthorId {
        equals: (docId: string) => boolean
    }
    export interface IEntry extends Document {
        _id: number
        length: number
        tags: string[]
        emojis: string[]
        content: string
        authorId: IAuthorId
        createdAt: string
        updatedAt: string
    }
    export interface IUser extends Document {
        role: string
        streak: number
        username: string
        email: string
    }
    export interface IJsonWebToken {
        uid: string,
        iat: string,
        role: string
    }
}

export namespace Email {
    export interface IEmailData {
        from: string
        to: string
        subject: string
        html: string
    }
}

export namespace Network {
    export interface IRequest extends Request {
        user: Models.IUser
    }
}

export namespace Errors {
    export interface IError {
        code: number
        message: string
        datetime?: string
    }

    export interface IPGError {
        message: string
        name: string
        length: number
        severity: string
        code: number
        detail: string
        hint: string
        position: string
        internalPosition: string | number
        internalQuery: string
        where: string
        schema: string
        table: string
        column: string
        dataType: string
        constraint: string
        file: string
        line: string
        routine: string
    }
}