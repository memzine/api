import * as mongoose from 'mongoose'

/**
 * Connect to mongodb.
 */

mongoose.connect(process.env.DB_URL)

export default mongoose