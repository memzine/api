import {DB_URL, PORT} from './config'

describe('Config', () => {
    test('Env Variables Should be Set', () => {
        expect(DB_URL).toBe(process.env.DB_URL)
        expect(DB_URL).toBe('postgres://memzine:43967Jonesy2@localhost:5432/test_memzine')
        expect(PORT).toBe(process.env.PORT)
        expect(PORT).toBe('1351')
    })
})