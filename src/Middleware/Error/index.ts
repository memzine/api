const errorHandler =  (err, req, res, next) => {
  if (err.send) {
    console.log(err)
    err.send(res)
  } else {
    console.log(err)
    res.status(500).send(err)
  }
}

export default errorHandler