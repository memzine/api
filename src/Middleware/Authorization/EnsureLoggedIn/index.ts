import { NextFunction, Request, Response } from 'express'
import NotAuthorizedError from '../../../Errors/Authentication/NotAuthorized'

const ensureLoggedIn = (req: Request, res: Response, next: NextFunction) => {
  if (req.user) {
    next()
  } else {
    const error = new NotAuthorizedError(new Error('You are not logged in.'))
    next(error)
    // error.send(res)
  }
}

export default ensureLoggedIn