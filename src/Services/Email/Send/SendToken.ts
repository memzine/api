import fetch from 'node-fetch'
import { Models, Email } from '../../../Typings/Types'
import { EMAIL_URL } from '../../../config'

const sendEmailToken = async (token: object, user: any) => {
  // post request to email service.
  // will return response from email server.
  const emailData: Email.IEmailData = {
    from: 'Theo <theo@memzine.co>',
    to: user.email,
    subject: 'Token time!',
    html: `<a>${token}</p>`
  }
  try {
    const result = fetch(EMAIL_URL, {
      method: 'POST',
      body: JSON.stringify({
        emailData
      })
    })
    return result
  } catch (error) {
    throw error
  }
}

export default sendEmailToken