import { Request, Response, NextFunction } from 'express'
import { Models, Network } from '../../../Typings/Types'
import User from '../../../Models/User/UserModel'
import verifyToken from '../Tokens/VerifyToken'
import NotAuthorizedError from '../../../Errors/Authentication/NotAuthorized'

/**
 * Middleware
 * 
 * Accepts a token in request body or query param ?token=<> (should be Authorization header later)
 * Verify token
 * Find user from token uid
 * Append user to req.body
 * 
 * if token verification fails, error
 * if user does not exist error.
 * -----------------------------------
 * @namespace ExpressMiddleware
 * @param req
 * @param res
 * @param next
 * -----------------------------------
 * req must contain token.
 * This will protect all protected routes.
 */

const authenticateUser = async (req: Network.IRequest, res: Response, next: NextFunction) => {
  try {
    const auth = req.get('x-auth')
    const token = await verifyToken(auth || req.query.token)
    const user = await User.findById(token.uid)
    req.user = user
    next() 
  } catch (error) {
    const err = new NotAuthorizedError(error)
    err.send(res)
  }
}

export default authenticateUser