import * as jwt from 'jsonwebtoken'
import { Models } from '../../../Typings/Types'
import { APP_SECRET } from '../../../config'
import { promisify } from 'util'

const jwtSignAsync = promisify(jwt.sign)

/**
 * Simply sign a token
 * Default exp of 30 days.
 * Can pass in custom exp.
 */

const signToken = async (user: Models.IUser, expiry?: string | number): Promise<object> => {
  return new Promise((resolve, reject) => {
    jwt.sign({
      uid: user._id,
      role: user.role,
      str: user.streak
    }, APP_SECRET, {
      expiresIn: expiry || '30 days'
    }, (err, token) => {
      if (err) {
        reject(err)
      } else {
        resolve(token)
      }
    })
  })
}

export default signToken