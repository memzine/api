import * as jwt from 'jsonwebtoken'
import { Models } from '../../../Typings/Types'
import { APP_SECRET } from '../../../config'
import { promisify } from 'util'


/**
 * Verifies a token
 * @argument token (a JWT)
 * @returns Promise<Models.IJsonWebToken>
 * @throws
 */

const jwtVerifyAsync: any = promisify(jwt.verify)

const verifyToken = async (token: string): Promise<Models.IJsonWebToken> => {
  try {
    return await jwtVerifyAsync(token, APP_SECRET) 
  } catch (error) {
    throw error
  }
}

export default verifyToken