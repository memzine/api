import * as dotenv from 'dotenv'

// Set the .env file according to NODE_ENV variable.
if (process.env.NODE_ENV === 'test') {
    dotenv.config({ path: './.env/test/.env' })
} else if (process.env.NODE_ENV === 'dev') {
    dotenv.config({ path: './.env/dev/.env' })
} else if (process.env.NODE_ENV === 'production') {
    dotenv.config({ path: './.env/prod/.env' })
}


export const PORT = process.env.PORT
export const DB_URL = process.env.DB_URL
export const APP_SECRET = process.env.APP_SECRET
export const EMAIL_URL = process.env.EMAIL_URL
export const SESSION_SECRET = process.env.SESSION_SECRET
export const BASE_URL = process.env.BASE_URL

// AuthO callback urls
// FACEBOOK
export const FACEBOOK_CB_URL = BASE_URL + '/auth/facebook/callback'
export const FACEBOOK_CLIENT_ID = process.env.FACEBOOK_CLIENT_ID
export const FACEBOOK_CLIENT_SECRET = process.env.FACEBOOK_CLIENT_SECRET

// TWITTER