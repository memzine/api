import { Request, Response, NextFunction } from 'express'
import { UserModels } from '../../Typings/Types'
import BadRequestError from '../../Errors/BadRequest'
import saveOne from '../../Infrastructure/Repositories/UserRepository/Methods/SaveOne/SaveOne'
import UserRepository from '../../Infrastructure/Repositories/UserRepository/UserRepository'

export default {
  addUser(req: Request, res: Response) {
    if (!req.body.email || !req.body.username) {
      new BadRequestError('You must provide an email and username.').send(res)
      return
    }
    const userObject: UserModels.IUserObject = {
      email: req.body.email,
      username: req.body.username
    }
    saveOne(userObject)
    .then((userDoc) => {
      res.status(200).send(userDoc)
    })
    .catch((error) => {
      res.status(500).send(error.message)
    })
  },
  // Get all users
  async getAllUsers(req: Request, res: Response, next: NextFunction) {
    try {
      const userList = await UserRepository.fetchAll(true)
      res.status(200).send(userList)
    } catch (error) {
      res.status(error.code || 500).send(error.message)
    }
  },

  async getOneById(req: Request, res: Response, next: NextFunction) {
    try {
      const user = await UserRepository.fetchOneById(req.user._id)
      if (user) {
        res.status(200).send(user)
      }
    } catch (error) {
        if (error.send) {
          error.send(res)
        } else {
          error.status(500).send(error)
        }
    }
  }

}