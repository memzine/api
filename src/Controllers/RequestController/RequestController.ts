import { Request, Response } from 'express'

class RequestController {
    private req: Request
    private res: Response
    private repository: any
    constructor(req: Request, res: Response, repository: any) {
        this.req = req
        this.res = res
        this.repository = repository
    }

    public async getOneById() {
        const doc = await this.repository.findById()
        this.res.send(doc.toString())
    }
}

export default RequestController