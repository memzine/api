import { Response, Request } from 'express'
import sendEmailToken from '../../Services/Email/Send/SendToken'
import userRepository from '../../Infrastructure/Repositories/UserRepository/UserRepository'
import BadRequestError from '../../Errors/BadRequest'
import signToken from '../../Services/Authentication/Tokens/SignToken'
import { Models } from '../../Typings/Types'
import NotFoundError from '../../Errors/NotFound'

/**
 * @expects EMAIL
 * Pretty big controller, does a lot.
 * It simply takes email in req.body
 * finds user,
 * sends token via email.
 */

const tokenController = {

  // @expects EMAIL
  postToken(req: Request, res: Response): void {
    const email = req.body.email

    // If email not provided, error.
    if (email === undefined || email === null) {
      return new BadRequestError('You must provide an email address.').send(res)
    }
    // Give access to prom return val user
    let user: Models.IUser

    try {
      userRepository.fetchOneByEmail(email)
        .then((usr) => {
          if (usr === null || usr === undefined) {
            const err = new NotFoundError('Could not find user.')
            throw err
          } else {
            user = usr
            return signToken(usr)
          }
        })
        .then((token) => {
          return sendEmailToken(token, user)
        })
        .then((emailResponse) => {
          res.status(200).send({ message: 'Email was sent.' })
        })
        .catch((error) => {
          if (typeof error.code === 'number' && error.code) {
            res.status(error.code).send(error)
          } else {
            res.status(500).send(error)
          }
        })
    } catch (error) {
      res.status(error.code || 500).send(error)
    }
  }
}

export default tokenController