import { Request, Response } from 'express'
import EntriesRepository from '../../Infrastructure/Repositories/EntriesRepository/EntriesRepository'
import RequestController from '../RequestController/RequestController'
import NotFoundError from '../../Errors/NotFound'
import BadRequestError from '../../Errors/BadRequest'
import { Models, Network, EntryModels } from '../../Typings/Types'
import WriteError from '../../Errors/ReadWrite/write'
import NotAuthorizedError from '../../Errors/Authentication/NotAuthorized'

const EntryController = {

    // Get All Entries
    async getAll(req: Network.IRequest, res: Response, next) {
        if (!req.user) {
            const err = new NotAuthorizedError(new Error('Not authenticated, no details present.'))
            // err.send(res)
            res.status(500).send(err)
            return
        }
        const user = req.user
        try {
            const entries: Models.IEntry[] = await EntriesRepository.fetchAll(user)
            res.status(200).send(entries)
            next()
        } catch (error) {
            new NotFoundError().send(res)
        }
    },

    // GET ONE ENTRY BY ID
    async getOne(req: Network.IRequest, res: Response) {
        const id = req.params.id
        const user = req.user
        try {
            const entry = await EntriesRepository.findById(id, user)
            res.send(entry)
        } catch (error) {
            error.send(res)
        }
    },

    async postOne(req: Network.IRequest, res: Response) {
        const entry: Models.IEntry = req.body.entry
        if (entry.authorId) {
            const err = new BadRequestError('You cannot pass in an authorId, the id is derived from your authentication token.')
            err.send(res)
            return
        }
        // Assign id from jwt token.
        const user: Models.IUser = req.user
        entry.authorId = user._id
        try {
            const response = await EntriesRepository.saveOne(entry)
            res.send(response)
        } catch (error) {
            new WriteError(error).send(res)
        }
    },
    // PATCH /entry
    patchOne(req: Network.IRequest, res: Response) {
        const user: Models.IUser = req.user
        const id: string = req.params.id
        const entry: EntryModels.IEntryObject = req.body.entry
        const entryObject = {
            content: entry.content,
            emojis: entry.emojis,
            tags: entry.tags
          }
        EntriesRepository.updateOne(id, entryObject, user)
        .then((updated) => {
            res.status(200).send(updated)
        })
        .catch((error) => {
            error.send(res)
        })
    }
}

export default EntryController