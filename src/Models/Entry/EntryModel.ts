import mongoose from '../../Database/db'
import { Models } from '../../Typings/Types'
const Types = mongoose.Schema.Types

const entrySchema: mongoose.Schema = new mongoose.Schema({
    title: {
        type: String,
        required: true,
        default: new Date().toLocaleString()
    },
    content: {
        type: Types.String,
        required: true
    },
    length: {
        type: Types.Number
    },
    createdAt: {
        type: Types.Date,
        default: Date.now
    },
    updatedAt: {
        type: Types.Date,
        default: Date.now
    },
    authorId: {
        type: Types.ObjectId,
        ref: 'User',
        required: true
    },
    tags: [{
        type: Types.String
    }],
    emojis: [{
        type: Types.String
    }]
})

const Entry: mongoose.Model<Models.IEntry> = mongoose.model('Entry', entrySchema)

export default Entry
