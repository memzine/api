import mongoose from '../../Database/db'
import { Model, Document } from 'mongoose'
import { Models } from '../../Typings/Types'
const Types = mongoose.Schema.Types

const userSchema: mongoose.Schema = new mongoose.Schema({
    email: {
      type: Types.String,
      required: true,
      unique: true,
      // select: false
    },
    username: {
      type: Types.String,
      required: true,
      unique: true
    },
    streak: {
      type: Types.Number,
      required: true,
      default: 0
    },
    role: {
      type: Types.String,
      required: true,
      default: 'user'
    },
    facebookProvider: {
      type: Object,
      // {
      //   id: String,
      //   token: String,
      //   name: String,
      //   username: String
      // }
      // select: false
    },
    twitterProvider: {
      type: {
        id: String,
        token: String,
        name: String,
        username: String
      },
      // select: false
    }
})

const User: Model<Models.IUser> = mongoose.model('User', userSchema)

export const hiddenFields = {
  email: 0,
  role: 0,
  facebookProvider: 0,
  twitterProvider: 0
}

export default User
