import * as passport from 'passport'
import * as FacebookTokenStrategy from 'passport-facebook-token'

import * as Express from 'express'
import * as session from 'express-session'
import { SESSION_SECRET, FACEBOOK_CB_URL, FACEBOOK_CLIENT_ID, FACEBOOK_CLIENT_SECRET } from '../../config'
import { Models } from '../../Typings/Types'
import UserRepository from '../../Infrastructure/Repositories/UserRepository/UserRepository'

/**
 * @link http://www.passportjs.org/docs/profile/    #session bit
 * 
 * serializeUser takes a user document - and passes it's id to done.
 * 
 * deserializeUser takes a user document and finds the user.
 * 
 * @help here: gosmlr.xyz/p5
 * 
 */

// serialize user (this decides what is stored in the session)
// We usually only put the id in here to avoid giving out sensitive info
// And to avoid having to update sessions when user data changes.
passport.serializeUser((user: Models.IUser, done) => {
  done(null, user._id)
})

// deserialize user
// Grabs user by id
// this finds user and attaches it to req as req.user
passport.deserializeUser((userId: string, done) => {
  UserRepository.fetchOneById(userId)
  .then((usrDoc) => {
    done(null, usrDoc)
  })
  .catch(err => {
    done(err, null)
  })
})

// Strategies
passport.use(new FacebookTokenStrategy(
  {
    clientID: FACEBOOK_CLIENT_ID,
    clientSecret: FACEBOOK_CLIENT_SECRET,
    // callbackURL: FACEBOOK_CB_URL,
    // profileFields: ['id', 'displayName', 'photos', 'email']
  }, (accessToken, refreshToken, profile, done) => {
    // console.log('HEY:', profile)
    UserRepository.fetchOneByFacebookIdOrCreate(profile)
    .then(userDoc => {
      done(null, userDoc)
    })
    .catch(error => {
      console.log(error)
      done(error)
    })
  }
))


// Express config here ----------------------- @param app (express app)
const passportConfig = (app: Express.Application): void => {
  
  // session config here.
  app.use(session({
    secret: SESSION_SECRET
  }))

  app.use(passport.initialize())
  app.use(passport.session())
}

export default passportConfig
export { passport }