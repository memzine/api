import CustomError from './CustomError'

class BadRequestError extends CustomError {
    constructor(message) {
        super()
        this.code = 400
        this.message = message
        this.name = 'Bad Request'
    }
}

export default BadRequestError