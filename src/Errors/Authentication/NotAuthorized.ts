import CustomError from '../CustomError'

class NotAuthorizedError extends CustomError {
    constructor(error) {
      super()
      this.code = 401
      this.message = error.message
      this.name = 'Unauthorized'
    }
}

export default NotAuthorizedError