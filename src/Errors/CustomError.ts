import { Errors } from '../Typings/Types'
import { Response } from 'express'


class CustomError extends Error {
    public message: string
    public code: number
    public datetime: string
    public name: string
    constructor() {
        super('An unknown error occurred.')
        this.name = 'CUSTOM ERROR'
        this.code = -1
        this.message = 'Error message UNDEFINED'
        this.datetime = new Date().toUTCString()
    }

    public print() {
        console.log(this.code, this.message)
    }

    public throw() {
        throw this
    }

    public send(res: Response) {
        res.status(this.code).send({ 
            error: this.name,
            message: this.message,
            code: this.code 
        })
    }

}

export default CustomError