import { Errors } from '../Typings/Types'
import CustomError from './CustomError'


class NotFoundError extends CustomError {
    
    constructor(message?: string) {
        super()
        this.name = 'NOT FOUND'
        this.code = 404
        this.message = message || 'The resource was not found.'
        this.datetime = new Date().toUTCString()
    }
}

export default NotFoundError