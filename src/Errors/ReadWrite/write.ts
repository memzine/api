import BadRequestError from '../BadRequest'
import { Errors } from '../../Typings/Types'
import { Response } from 'express'
import { Error } from 'mongoose'

class WriteError extends BadRequestError {
    constructor(error: Error) {
        super(error.message)
        this.code = 400
        this.name = error.name
    }

    public send(res: Response) {
        res.status(this.code).send({
            error: this.name,
            message: this.message
        })
    }
}

export default WriteError