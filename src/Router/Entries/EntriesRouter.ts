import * as express from 'express'
import * as bodyParser from 'body-parser'
import EntryController from '../../Controllers/EntryController/EntryController'
import errorHandler from '../../Middleware/Error/index'


const entriesRouter = express.Router()
entriesRouter.use(bodyParser.json())
entriesRouter.use(errorHandler)

// GET /entries
entriesRouter.get('/', EntryController.getAll)

// GET /entries/{id}
entriesRouter.get('/:id', EntryController.getOne)

// POST /entries
entriesRouter.post('/', EntryController.postOne)

// PATCH /entry
entriesRouter.patch('/:id', EntryController.patchOne)

export default entriesRouter