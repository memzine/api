import * as express from 'express'
import * as bodyParser from 'body-parser'
import userController from '../../Controllers/UserController/UserController'
import ensureLoggedIn from '../../Middleware/Authorization/EnsureLoggedIn'

const userRouter = express.Router()

userRouter.use(bodyParser.json())
userRouter.use(ensureLoggedIn)


// Expects Email
userRouter.post('/', userController.addUser)

userRouter.get('/', userController.getAllUsers)

userRouter.get('/me', userController.getOneById)

export default userRouter