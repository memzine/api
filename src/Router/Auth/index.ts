import * as express from 'express'
import * as bodyParser from 'body-parser'
import { passport } from '../../Authentication/Passport'

const authRouter = express.Router()
authRouter.use(bodyParser.json())


function setAccessToken(req, res, next) {
  req.body.access_token = req.body.code
  next()
}

// GET /auth/facebook
// authRouter.post('/facebook', passport.authenticate('facebook-token', {
//   scope: ['email', 'public_profile']
// }))
// passport.authenticate('facebook-token'),
authRouter.get('/facebook', passport.authenticate('facebook-token'), (req, res) => {
  if (req.user) {
    res.send(req.user)
    // console.log(req)
  } else {
    res.status(401).send('wrong')
  }
})

// GET /auth/facebook/callback
authRouter.get('/facebook/callback', 
  passport.authenticate('facebook-token', { failureRedirect: '/login' }),
  (req: express.Request, res: express.Response) => {
    res.redirect('/auth/profile')
  }
)

authRouter.get('/profile', (req: express.Request, res: express.Response) => {
  if (req.user) {
    res.send(`<pre>${JSON.stringify(req.user, undefined, 2)}</pre>`)
  } else {
    res.status(404).send('no')
  }
})

export default authRouter