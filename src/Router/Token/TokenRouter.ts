import * as express from 'express'
import * as bodyParser from 'body-parser'

import tokenController from '../../Controllers/TokenController/TokenController'

const tokenRouter = express.Router()

tokenRouter.use(bodyParser.json())

// Expects Email
tokenRouter.post('/', tokenController.postToken)

export default tokenRouter