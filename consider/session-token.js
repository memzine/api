/**
 * 
 * @param {express.request} req 
 * @param {express.res} res 
 * @param {express.next} next 
 * 
 * Authentication middleware for both jwts & sessions.
 */

const authenticate = async (req, res, next) => {
  // if a token is provided (use authorization header in real)
  if (req.body.token || req.params.token) {
    // validate token
    const valid = await validateToken()
    if (valid) {
      // run checks against user object in jwt payload
      next()
    } else {
      // throw error and send
    }
    // otherwise if session is provided
  } else if (req.session && req.session.username) {
    // run checks against user
  } else {
    // Fail out because neither a token nor a session exists.
  }
}